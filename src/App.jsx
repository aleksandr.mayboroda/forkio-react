import FirstBlock from './components/business/FirstBlock/FirstBlock'
import SecondBlock from './components/business/SecondBlock/SecondBlock'
import ThirdBlock from './components/business/ThirdBlock/ThirdBlock'
import FourthBlock from './components/business/FourthBlock/FourthBlock'

const App = () => {
  return (
    <div className="container">
      <FirstBlock />
      <SecondBlock />
      <ThirdBlock />
      <FourthBlock />
    </div>
  )
}

export default App
