import React, { useCallback } from 'react'

import styles from './header.module.scss'

import Logo from '../../chunks/Logo/Logo'
import Button from '../../chunks/Button/Button'
import TopMenu from '../TopMenu/TopMenu'

const Header = () => {
  const btnClick = useCallback(() => console.log('header btn click'), [])

  return (
    <header className={styles.header}>
      <div className={`container-inner ${styles['header__list']}`}>
        <Logo />
        <div className={styles['header__btn-block']}>
          <Button
            text={'buy now'}
            className={styles['header__btn']}
            onClick={btnClick}
          />
        </div>
        <TopMenu />
      </div>
    </header>
  )
}

export default Header
