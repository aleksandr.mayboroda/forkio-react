import React from 'react'

import styles from './featurelist.module.scss'

import Feature from '../../chunks/Feature/Feature'

const featureList = [
  {
    title: 'Creatred to Make The Web Better',
    text: 'Aenean cursus imperdiet nisl id fermentum. Aliquam pharetra duilaoreet vulputate dignissim. Sed id metus id quam auctor molestie egetut augue.',
    image: './images/features/feature1.png',
  },
  {
    title: 'Incredibly Powerful Tool',
    text: 'Maecenas eu dictum felis, a dignissim nibh. Mauris rhoncus felis odio, ut volutpat massa placerat ac. Curabitur dapibus iaculis mi gravida luctus. Aliquam erat volutpat.',
    image: './images/features/feature2.png',
  },
  {
    title: 'Experimental Stuff',
    text: 'Maecenas eu dictum felis, a dignissim nibh. Mauris rhoncus felis odio, ut volutpat massa placerat ac. Curabitur dapibus iaculis mi gravida luctus. Aliquam erat volutpat.',
    image: './images/features/feature3.png',
  },
  {
    title: 'Creatred to Make The Web Better 123',
    text: 'Maecenas eu dictum felis, a dignissim nibh. Mauris rhoncus felis odio, ut volutpat massa placerat ac. Curabitur dapibus iaculis mi gravida luctus. Aliquam erat volutpat.',
    image: './images/features/feature4.png',
  },
]

const FeatureList = () => {
  const list =
    !featureList.length > 0
      ? null
      : featureList.map((item) => <Feature {...item} key={item.title} />)

  return <div className={styles['feature-list']}>{list}</div>
}

export default FeatureList
