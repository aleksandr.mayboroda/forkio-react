import { useState, useCallback } from 'react'

import styles from './topmenu.module.scss'

const menuElements = [
  { name: 'overview' },
  { name: 'about fork' },
  { name: 'buying options' },
  { name: 'support' },
]

const TopMenu = () => {
  const [showMobileMenu, setShowMobileMenu] = useState(false)

  const toggleMobileMenu = useCallback(
    (ev) => setShowMobileMenu((prev) => !prev),
    []
  )

  const mobileList =
    !menuElements.length > 0
      ? null
      : menuElements.map(({ name }) => (
          <a href="/" className={styles['mobile-menu__item']} key={name}>
            {name}
          </a>
        ))

  const desctopList =
    !menuElements.length > 0
      ? null
      : menuElements.map(({ name }) => (
          <a href="/" className={styles['desctop-menu__item']} key={name}>
            {name}
          </a>
        ))

  return (
    <div className={styles['top-menu-block']}>
      <input
        type="checkbox"
        className={styles['top-menu-block__checkbox']}
        checked={showMobileMenu}
        onChange={toggleMobileMenu}
        id="top_menu_swithcer"
      />
      <label
        htmlFor="top_menu_swithcer"
        className={`${styles['top-menu-block__mobile-btn']}`}
        // onClick={toggleMobileMenu}
      >
        <span className={`${styles['top-menu-block__middle-line']}`} />
      </label>
      {showMobileMenu && (
        <nav className={styles['mobile-menu']}>{mobileList}</nav>
      )}
      <nav className={styles['desctop-menu']}>{desctopList}</nav>
    </div>
  )
}

export default TopMenu
