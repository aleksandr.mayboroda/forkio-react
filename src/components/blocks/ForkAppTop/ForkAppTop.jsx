import { useCallback } from 'react'

import styles from './forkapptop.module.scss'

import Header from '../Header/Header'
import Button from '../../chunks/Button/Button'

const ForkAppTop = () => {
  const btnClick = useCallback(() => console.log('bottom btn click'), [])

  return (
    <div
      className={styles['fork-app-top']}
      style={{ backgroundImage: 'url(./images/header/header_bg.png)' }}
    >
      <Header />
      <div className={`container-inner ${styles['fork-app-top__content']}`}>
        <h1 className={styles['fork-app-top__title']}>fork app</h1>
        <p className={styles['fork-app-top__sub-title']}>
          A Real Gamechanger In The World Of Web-Development
        </p>
        <p className={styles['fork-app-top__text']}>
          v. 2.8 For Mac and Windows
        </p>
      </div>
      <Button
        text="Download For Free Now"
        className={styles['fork-app-top__btn']}
        onClick={btnClick}
      />
    </div>
  )
}

export default ForkAppTop
