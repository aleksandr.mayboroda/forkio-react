import React from 'react'

import styles from './features.module.scss'

import RevolutionaryFeaturesTitle from '../../chunks/RevolutionaryFeaturesTitle/RevolutionaryFeaturesTitle'
import FeatureList from '../FeatureList/FeatureList'
import RevolutionaryImage from '../../chunks/RevolutionaryImage/RevolutionaryImage'

const RevolutionaryFeatures = () => {
  return (
    <div className={styles['revolutionary-features']}>
      <div
        className={`container-inner ${styles['revolutionary-features__block']} `}
      >
        <RevolutionaryFeaturesTitle
          className={styles['revolutionary-features__title']}
        />
        <FeatureList />
        <RevolutionaryImage />
      </div>
    </div>
  )
}

export default RevolutionaryFeatures
