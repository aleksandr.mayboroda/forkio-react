import React from 'react'

import styles from './servicelist.module.scss'

const ServiceList = ({ services }) => {
  const serviceList =
    !services.length > 0
      ? null
      : services.map((item) => (
          <li key={item} className={styles['services-list__item']}>
            {item}
          </li>
        ))

  return <ul className={styles['services-list']}>{serviceList}</ul>
}

export default ServiceList
