import React from 'react'

import styles from './forksubscription.module.scss'

import SubscriptionList from '../SubscriptionList/SubscriptionList'

const ForkSubscription = () => {
  return (
    <div
      className={styles['fork-subscription']}
      style={{ backgroundImage: 'url(./images/subscription/subscription_bg1.png)' }}
    >
      <h2 className={`section-title ${styles['fork-subscription__title']}`}>Fork Subscription Pricing</h2>
      <SubscriptionList />
    </div>
  )
}

export default ForkSubscription
