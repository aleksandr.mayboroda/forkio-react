import React from 'react'

import styles from './gitbuttonlist.module.scss'

import Icon from '../../chunks/Icon/Icon'
import GitButton from '../../chunks/GitButton/GitButton'

const GitButtonList = () => {
  return (
    <div className={styles['gitbutton-list']}>
      <GitButton
        className={styles['gitbutton-list__item']}
        text="watch"
        number="1,397"
        icon={<Icon type="bell" width="17" height="17" />}
      />
      <GitButton
        className={styles['gitbutton-list__item']}
        text="watch"
        number="1,397"
        icon={<Icon type="fork" width="17" height="17" />}
      />
      <GitButton
        className={styles['gitbutton-list__item']}
        text="watch"
        number="1,397"
        icon={<Icon type="star" width="17" height="17" />}
      />
    </div>
  )
}

export default GitButtonList
