import React from 'react'

import styles from './revolutionary.module.scss'

import GitButtonList from '../GitButtonList/GitButtonList'

const Revolutionary = () => {
  return (
    <div className={styles['revolutionary']}>
      <h2 className={styles['revolutionary__title']}>Revolutionary editor</h2>
      <p className={styles['revolutionary__text']}>
        Aenean cursus imperdiet nisl id fermentum. Aliquam pharetra dui laoreet
        vulputate dignissim. Sed id metus id quam auctor molestie eget ut augue.
      </p>
      <GitButtonList />
    </div>
  )
}

export default Revolutionary
