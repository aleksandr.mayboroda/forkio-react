import React from 'react'

import styles from './subscriptionlist.module.scss'

import SubscriptionItem from '../../chunks/SubscriptionItem/SubscriptionItem'

const subscriptionList = [
  {
    title: 'students',
    price: 29,
    currency: '$',
    services: ['personal license'],
    isActive: false,
  },
  {
    title: 'professional',
    price: 59,
    currency: '$',
    services: ['professional License', 'email support'],
    isActive: false,
  },
  {
    title: 'agency',
    price: 99,
    currency: '$',
    services: ['1-12 Team Members', 'hone support'],
    isActive: true,
  },
  {
    title: 'enterprise',
    price: 159,
    currency: '$',
    services: ['Unlimited Team Members', '24/ 7 phone support'],
    isActive: false,
  },
]

const SubscriptionList = () => {
  const list =
    !subscriptionList.length > 0
      ? null
      : subscriptionList.map((item) => (
          <div key={item.title} className={styles['subscription-list__item']}>
            <SubscriptionItem {...item} />
          </div>
        ))
  return <div className={styles['subscription-list']}>{list}</div>
}

export default SubscriptionList
