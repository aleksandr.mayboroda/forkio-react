import React from 'react'

import styles from './forkappbottom.module.scss'

const ForkAppBottom = () => {
  return (
    <div className={styles['fork-app-bottom']}>
      <p className={styles['fork-app-bottom__text']}>
        Unlimited 30-Days Trial Period
      </p>
    </div>
  )
}

export default ForkAppBottom
