import React from 'react'

import styles from './clientlist.module.scss'

import Client from '../../chunks/Client/Client'

const clientList = [
  {
    name: 'smashing',
    text: 'Sed vestibulum scelerisque urna, eu finibus leo facilisis sit amet. Proin id dignissim magna. Sed varius urna et pulvinar venenatis.',
    image: './images/clients/client1.png',
  },
  {
    name: 'codrops',
    text: 'Donec euismod dolor ut ultricies consequat. Vivamus urna ipsum, rhoncus molestie neque ac, mollis eleifend nibh.',
    image: './images/clients/client2.png',
  },
  {
    name: 'w',
    text: 'In efficitur in velit et tempus. Duis nec odio dapibus, suscipit erat fringilla, imperdiet nibh. Morbi tempus auctor felis ac vehicula.',
    image: './images/clients/client3.png',
  },
  {
    name: 'pixel',
    text: 'Sed vestibulum scelerisque urna, eu finibus leo facilisis sit amet. Proin id dignissim magna. Sed varius urna et pulvinar venenatis.',
    image: './images/clients/client4.png',
  },
  {
    name: 'creative',
    text: 'Praesent ut eros tristique, malesuada lectus vel, lobortis massa. Nulla faucibus lorem id arcu consequat faucibus.',
    image: './images/clients/client5.png',
  },
  {
    name: 'tnw',
    text: 'Fusce pharetra erat id odio blandit, nec pharetra eros venenatis. Pellentesque porttitor cursus massa et vestibulum.',
    image: './images/clients/client6.png',
  },
]

const ClientList = () => {
  const list =
    !clientList.length > 0
      ? null
      : clientList.map((item) => (
          <Client
            key={item.name}
            {...item}
            className={styles['client-list__item']}
          />
        ))
  return <div className={styles['client-list']}>{list}</div>
}

export default ClientList
