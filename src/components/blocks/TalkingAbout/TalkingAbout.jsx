import React from 'react'

import styles from './talkingabout.module.scss'

import ClientList from '../ClientList/ClientList'

const TalkingAbout = () => {
  return (
    <div
      className={styles['talking-about']}
      style={{ backgroundImage: 'url(./images/people/people_bg1.png)' }}
    >
      <h2 className={`section-title ${styles['talking-about__title']}`}>
        People Are Talking About Fork
      </h2>
      <ClientList />
      <p className={styles['talking-about__text']}>
        Duis lobortis arcu sed arcu tincidunt feugiat. Nulla nisi mauris,
        facilisis vitae aliquet id, imperdiet quis nibh. Donec eget elit eu
        libero tincidunt consequat nec elementum orci. Cum sociis natoque
        penatibus et magnis dis parturient montes, nascetur ridiculus mus.
      </p>
    </div>
  )
}

export default TalkingAbout
