import { useRef } from 'react'
import useHover from '../../../hooks/useHover'

import styles from './logo.module.scss'

const Logo = () => {
  const logoRef = useRef()
  const isHovering = useHover(logoRef)
  return (
    <a className={styles.logo} href="/" ref={logoRef}>
      <p
        className={styles['logo__image']}
        style={{
          backgroundImage: isHovering
            ? 'url(./images/header/logo_hovered.png)'
            : 'url(./images/header/logo.png)',
        }}
      ></p>

      <p
        className={`${styles['logo__text']} ${
          isHovering && styles['logo__text--hovered']
        }`}
      >
        ForkIO
      </p>
    </a>
  )
}

export default Logo
