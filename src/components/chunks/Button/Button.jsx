import React from 'react'

import styles from './button.module.scss'

const Button = ({ text, className, onClick }) => {
  return (
    <button className={`${styles.btn} ${className}`} onClick={onClick}>
      {text}
    </button>
  )
}

export default Button
