import React from 'react'

import styles from './revoimage.module.scss'

import RevolutionaryFeaturesTitle from '../RevolutionaryFeaturesTitle/RevolutionaryFeaturesTitle'

const RevolutionaryImage = () => {
  return (
    <div className={styles['revolutionary-image']}>
      <RevolutionaryFeaturesTitle className={styles['revolutionary-image__title']} />
      <div className={styles['revolutionary-image__wrapper']}>
        <img src="./images/feature/feature_img.png" alt="feature" />
      </div>
    </div>
  )
}

export default RevolutionaryImage
