import React from 'react'

import styles from './feature.module.scss'

const Feature = ({ title, text, image }) => {
  return (
    <div className={styles['feature']}>
      <div className={styles['feature__image']}>
        <img src={image} alt={title} />
      </div>
      <div className={styles['feature__info']}>
        <h5 className={styles['feature__title']}>{title}</h5>
        <p className={styles['feature__text']}>{text}</p>
      </div>
    </div>
  )
}

export default Feature
