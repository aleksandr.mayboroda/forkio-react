import React from 'react'

import styles from './subscriptionitem.module.scss'

import ServiceList from '../../blocks/ServiceList/ServiceList'
import Button from '../Button/Button'

const SubscriptionItem = ({ title, currency, price, services = [], isActive }) => {

  return (
    <div className={`${styles['subscription-item']} ${isActive ? styles['subscription-item--active'] : '' }`}>
      <h5 className={styles['subscription-item__title']}>{title}</h5>
      <h4 className={styles['subscription-item__price']}>
        {currency}
        <span className={styles['subscription-item__price-price']}>
          {price}
        </span>
      </h4>
      <p className={styles['subscription-item__text']}>per month</p>
      <ServiceList services={services} />
      <Button className={styles['subscription-item__btn']} text="purchase" onClick={() => console.log('clicked service', title)}/>
    </div>
  )
}

export default SubscriptionItem
