import React from 'react'

import styles from './gitbutton.module.scss'

const GitButton = ({className, text, icon, number }) => {
  return (
    <button className={`${styles['git-button']} ${className}`}>
      <span className={styles["git-button__left"]}>
        <span className={styles['git-button__icon']}>{icon}</span>
        <span className={styles['git-button__text']}>{text}</span>
      </span>
      <span className={styles['git-button__number']}>{number}</span>
    </button>
  )
}

export default GitButton
