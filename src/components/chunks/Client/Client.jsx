import React from 'react'

import styles from './client.module.scss'

const Client = ({ image, text, className }) => {
  return (
    <div className={`${styles.client} ${className}`}>
      <div className={styles['client__image']}>
        <img src={image} alt={text} />
      </div>
      <p className={styles['client__text']}>{text}</p>
    </div>
  )
}

export default Client
