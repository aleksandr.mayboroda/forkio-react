import React from 'react'

import styles from './revtitle.module.scss'

const RevolutionaryFeaturesTitle = ({ className }) => {
  return (
    <h3 className={`${styles['revolutionary-features-title']} ${className}`}>
      &lt;Here is what you get&gt;
    </h3>
  )
}

export default RevolutionaryFeaturesTitle
