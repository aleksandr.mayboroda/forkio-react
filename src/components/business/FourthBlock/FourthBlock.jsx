import React from 'react'

import ForkSubscription from '../../blocks/ForkSubscription/ForkSubscription'

const FourthBlock = () => {
  return (
    <ForkSubscription />
  )
}

export default FourthBlock