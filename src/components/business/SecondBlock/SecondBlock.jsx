import React from 'react'

import styles from './secondblock.module.scss'

import Revolutionary from '../../blocks/Revolutionary/Revolutionary'
import RevolutionaryFeatures from '../../blocks/RevolutionaryFeatures/RevolutionaryFeatures'

const SecondBlock = () => {
  return (
    <div className={styles['second-block']}>
      <Revolutionary />
      <RevolutionaryFeatures />
    </div>
  )
}

export default SecondBlock
