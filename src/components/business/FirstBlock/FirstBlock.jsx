import React from 'react'

import ForkAppTop from '../../blocks/ForkAppTop/ForkAppTop'
import ForkAppBottom from '../../blocks/ForkAppBottom/ForkAppBottom'

const FirstBlock = () => {
  return (
    <section>
      <ForkAppTop />
      <ForkAppBottom />
    </section>
  )
}

export default FirstBlock
