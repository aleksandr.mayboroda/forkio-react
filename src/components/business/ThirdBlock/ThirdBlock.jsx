import React from 'react'

import TalkingAbout from '../../blocks/TalkingAbout/TalkingAbout'

const ThirdBlock = () => {
  return <TalkingAbout />
}

export default ThirdBlock
