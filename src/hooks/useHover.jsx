import { useState, useEffect } from 'react'

const useHover = (ref) => {
  const [isHovering, setIsHovering] = useState(false)
  const on = () => setIsHovering(true)
  const off = () => setIsHovering(false)

  useEffect(() => {
    if (!ref.current) {
      return
    }
    const node = ref.current
    node.addEventListener('mousenter', on)
    node.addEventListener('mousemove', on)
    node.addEventListener('mouseleave', off)

    return () => {
      node.removeEventListener('mousenter', on)
      node.removeEventListener('mousemove', on)
      node.removeEventListener('mouseleave', off)
    }
  })

  return isHovering
}

export default useHover